How to get the xs value from Facebook cookie with Google Chrome:

1. Open www.messenger.com
2. Open Developer Tools 
3. Click "Resouces" tab
4. Click, "Cookies" in the left panel
5. Click "www.messenger.com"
6. Copy the value of "xs"

**Word of warning**

This value is to be used in `fb-sleep-stats` config file (development.json). But PLEASE do not share this with anyone else, since it provides access to your Facebook account.
